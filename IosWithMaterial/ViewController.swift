//
//  ViewController.swift
//  IosWithMaterial
//
//  Created by Syncrhonous on 14/7/19.
//  Copyright © 2019 Syncrhonous. All rights reserved.
//

import UIKit
import MaterialComponents.MaterialTextFields


class ViewController: UIViewController {


    @IBOutlet weak var mTextField: MDCTextField!
    var textFieldControllerFloating = MDCTextInputControllerUnderline()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
       // mTextField.delegate = (self as! UITextFieldDelegate)
        textFieldControllerFloating = MDCTextInputControllerUnderline(textInput: mTextField)
        mTextField.isUserInteractionEnabled = true
        //mTextField.clearButtonMode = .whileEditing
        
        textFieldControllerFloating.activeColor = UIColor.orange                            // active label & underline
        textFieldControllerFloating.normalColor = UIColor.lightGray                         // default underline
        textFieldControllerFloating.errorColor = UIColor.red
        
        textFieldControllerFloating.leadingUnderlineLabelTextColor = UIColor.darkGray       // The helper text
        textFieldControllerFloating.trailingUnderlineLabelTextColor = UIColor.green
        textFieldControllerFloating.floatingPlaceholderActiveColor = UIColor.red              // inline label
        textFieldControllerFloating.borderFillColor = UIColor.white
        
    }


}

